import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { actionCreators } from ".";
import reducers from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";

export const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);
