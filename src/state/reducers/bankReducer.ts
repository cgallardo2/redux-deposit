import reducers from ".";
import { ActionType } from "../Action-types";
import { Action } from "../Actions";

 const initialState =0;

//  interface Action{
//     type:string,
//     payload?:number,
//  }

 const reducer  = (state:number= initialState,action:Action) => {
    switch (action.type){
        case ActionType.DEPOSIT:
            return state + action.payload;
        case ActionType.WITHDRAW:
            return state - action.payload;
        
        case ActionType.BANKRUPT:
            return 0;
        default:
            return state;
    }
}

export default reducer

