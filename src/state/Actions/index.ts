import {ActionType} from "../Action-types/index"
interface DepositAction {
  type: ActionType.DEPOSIT;
  payload: number;
}
interface WithdrawAction {
  type: ActionType.WITHDRAW;
  payload: number;
}
interface BankruptAction {
  type: ActionType.BANKRUPT;
}
export type Action = DepositAction | WithdrawAction | BankruptAction;


