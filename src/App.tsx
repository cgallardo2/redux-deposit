import React, { useState } from "react";
import logo from "./logo.svg";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators, State } from "./state";

function App() {
  const [input, setInput] = useState(5); // '' is the initial state value
  const dispatch = useDispatch();
  const { depositMoney, withdrawMoney, bankrupt } = bindActionCreators(
    actionCreators,
    dispatch
  );
  const amount = useSelector((state: State) => state.bank);
  console.log("state of the bank: " + amount);
  return (
    <div className=" text-center mx-auto Container" style={{ width: "800px" }}>
      <h1 style={{ margin: "100px  " }} className="display-4">
        Redux set counter
      </h1>
      <h4>Insert the amount you want to transact greater than 0</h4>
      <input
        className="form-control"
        type="number"
        value={input}
        onInput={(e: React.KeyboardEvent<HTMLInputElement>) => {
          setInput(e.currentTarget.valueAsNumber || input);
          console.log(input);
        }}
      />

      <h1 className="display-1 font-weight-bold">{amount}</h1>
      <div className="col-xs-3">
        <button
          style={{ marginRight: "5px", marginBottom: "5px" }}
          className="btn btn-outline-success  btn-lg btn-block"
          onClick={() => depositMoney(input)}
        >
          Deposit
        </button>
        <button
          style={{ marginRight: "5px", marginBottom: "5px" }}
          className="btn btn-outline-warning btn-lg btn-block"
          onClick={() => withdrawMoney(input)}
        >
          Withdraw
        </button>
        <button
          style={{ marginRight: "5px", marginBottom: "5px" }}
          className="btn btn-outline-danger btn-lg btn-block"
          onClick={() => bankrupt()}
        >
          Reset
        </button>
      </div>
    </div>
  );
}

export default App;
